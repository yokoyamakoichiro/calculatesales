<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="signup">ユーザー新規登録</a>
			</c:if>
		</div>

		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				<c:out value="${loginUser.account}" />
			</div>
			<div class="account">
				<c:out value="${loginUser.description}" />
			</div>
		</div>
		<table border="1">
			<tr>
				<th>アカウント名</th>
				<th>ログインID</th>
				<th>支店コード</th>
				<th>部署役職</th>
			</tr>


			<c:forEach items="${inputusers}" var="users">
				<div class="users">
					<tr>
						<div class="account-department_name">
							<td><span class="アカウント名"><c:out
										value="${users.account}" /></span>
							<td><span class="ログインID"><c:out
										value="${users.loginid}" /></span>
							<td><span class="支店コード"><c:out
										value="${users.branch_name}" /></span>
							<td><span class="部署.役職"><c:out
										value="${users.department_name}" /></span>
						</div>
						<form action="useredit" method="get">
							<input name="id" value="${users.id}" id="id" type="hidden" /> <input
								type="submit" value="編集">

						</form>
						<form action="index.jsp" method="post">
							<c:choose>
								<c:when test="${users.use_data == 0}">
									<input type="hidden" name="id" value="${users.id}">
									<input type="hidden" name="use_data" value="1">
									<input type="submit" value="復活"
										onClick="return confirm ('アカウントを復活しますか？')" />
								</c:when>
								<c:when test="${users.use_data == 1}">
									<input type="hidden" name="id" value="${users.id}">
									<input type="hidden" name="use_data" value="0">
									<input type="submit" value="停止"
										onClick="return confirm ('アカウントを停止しますか？')" />
								</c:when>
							</c:choose>
						</form>
					</tr>
				</div>
			</c:forEach>
		</table>
	</div>
<body>
	<br />
</body>
<div class="copylight">Copyright(c)YourName</div>
</body>
</html>