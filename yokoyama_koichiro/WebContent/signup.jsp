<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="loginid">ログインID</label> <input name="loginid"
				id="loginid" value="${user.loginid}" /> <br /> <label
				for="account">アカウント名</label> <input name="account" id="account"
				value="${user.account}" /> <br /> <label for="password">登録パスワード</label>
			<input name="password" type="password" id="password" /> <br /> <label
				for="cherkpassword">確認パスワード</label> <input name="cherkpassword"
				type="password" id="cherkpassword" /> <br /> <select name="branch">
				<label for="branch">支店名</label>
				<c:choose>
					<c:when test="${user.branch== null}">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${user.branch==1}">
						<option value="1" selected>本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${user.branch==2}">
						<option value="1">本社1</option>
						<option value="2" selected>支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${user.branch==3}">
						<option value="1">本社1</option>
						<option value="2">支店A</option>
						<option value="3" selected>支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${user.branch==4}">
						<option value="1">本社1</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4" selected>支店C</option>
					</c:when>
				</c:choose>
			</select><br /> <br /> <select name="department">
				<br />
				<label for="department">役職,部署</label>
				<c:choose>
					<c:when test="${user.department == null}">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${user.department ==1}">
						<option value="1" selected>総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${user.department ==2}">
						<option value="1">総務人事担当者</option>
						<option value="2" selected>情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${user.department ==3}">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3" selected>支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${user.department ==4}">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4" selected>社員</option>
					</c:when>
				</c:choose>
			</select><br /> <br /> <input type="submit" value="登録" /> <br /> <a
				href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>
