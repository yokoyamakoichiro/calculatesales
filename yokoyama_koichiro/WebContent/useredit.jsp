<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録情報一覧</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="useredit" method="post">
			<input type="hidden" name="id" value="${editUser.id}"> <br />
			<label for="loginid">ログインID</label> <br /> <input name="loginid"
				value="${editUser.loginid}" id="loginid" /><br /> <br /> <label
				for="account">アカウント名</label> <br /> <input name="account"
				value="${editUser.account}" /><br /> <br /> <label for="password">登録パスワード</label>
			<br /> <input name="password" type="password" id="password" /> <br />
			<br /> <label for="cherkpassword">確認パスワード</label> <br /> <input
				name="cherkpassword" type="password" id="cherkpassword" /> <br />
			<select name="branch">
				<br />
				<label for="branch">支店名</label>
				<br />
				<c:choose>
					<c:when test="${editUser.branch == 1}">
						<option value="1" selected>本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${editUser.branch == 2}">
						<option value="1">本社</option>
						<option value="2" selected>支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${editUser.branch == 3}">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3" selected>支店B</option>
						<option value="4">支店C</option>
					</c:when>
					<c:when test="${editUser.branch == 4}">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4" selected>支店C</option>
					</c:when>
				</c:choose>
			</select><br /> <select name="department">
				<br />
				<label for="department">役職,部署</label>
				<br />
				<c:choose>

					<c:when test="${editUser.department == 1}">
						<option value="1" selected>総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${editUser.department == 2}">
						<option value="1">総務人事担当者</option>
						<option value="2" selected>情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${editUser.department == 3}">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3" selected>支店長</option>
						<option value="4">社員</option>
					</c:when>
					<c:when test="${editUser.department == 4}">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者A</option>
						<option value="3">支店長</option>
						<option value="4" selected>社員</option>
					</c:when>
				</c:choose>
			</select><br /> <br /> <input type="submit" value="登録" />
		</form>
		<br /> <a href="./">戻る</a>
		<div class="copylight">Copyright(c)YourName</div>
	</div>
</body>
</html>