package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,

			HttpServletResponse response) throws IOException, ServletException {
		List<String> message = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, message) == true) {
			User user = new User();
			user.setLoginid(request.getParameter("loginid"));
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setPassword(request.getParameter("cherkpassword"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));

			new UserService().register(user);

			response.sendRedirect("./");

		} else {

			session.setAttribute("errorMessages", message);

			User user = new User();
			user.setLoginid(request.getParameter("loginid"));
			user.setAccount(request.getParameter("account"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String cherkpassword = request.getParameter("cherkpassword");
		String loginid = request.getParameter("loginid");
		int user = new UserService().check(loginid);

		if (StringUtils.isEmpty(account)) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");
		}

		if (!(password.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");

		}

		if (account.length() > 10) {
			messages.add("アカウントは10文字以下で入力してください");
		}

		if (!(loginid.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}

		// パスワードの一致確認
		if (!Objects.equals(password, cherkpassword)) {
			// 一致していなかったら、エラーメッセージを表示する
			messages.add("パスワードと確認用パスワードが一致しません");
		}

		//ログインID重複確認

		if (user == 1) {
			messages.add("すでに使われているログインIDです");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}