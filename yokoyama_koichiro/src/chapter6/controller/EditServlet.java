package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Edit;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.EditService;

@WebServlet(urlPatterns = { "/useredit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//iDを取得
		int Id = Integer.parseInt(request.getParameter("id"));
		//idを元にDBからユーザー情報取得
		Edit editUser = new EditService().getUser(Id);
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("useredit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		Edit user = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {

				String password = request.getParameter("password");
				String cherkpassword = request.getParameter("cherkpassword");

				if (StringUtils.isBlank(password) && StringUtils.isBlank(cherkpassword)) {
					new EditService().update2(user);//変更かけない
				} else {
					new EditService().update(user);
				}

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", user);
				request.getRequestDispatcher("useredit.jsp").forward(request, response);

				return;
			}

			session.setAttribute("id", user);

			response.sendRedirect("./");
		} else {

			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("useredit.jsp").forward(request, response);
		}
	}

	private Edit getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		Edit user = new Edit();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginid(request.getParameter("loginid"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setDepartment(Integer.parseInt(request.getParameter("department")));
		return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int checkid = Integer.parseInt(request.getParameter("id"));
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String cherkpassword = request.getParameter("cherkpassword");
		String loginid = request.getParameter("loginid");
		String checkloginid = request.getParameter("loginid");
		Edit user = new EditService().idcheck(checkloginid, checkid);

		if (StringUtils.isEmpty(account)) {
			messages.add("アカウント名を入力してください");
		}

		if (!StringUtils.isBlank(password)) {
			if (!(password.matches("[0-9a-zA-Z]{6,20}")))
				messages.add("パスワードは6文字以上20文字以下で入力してください");

		}

		if (account.length() > 10) {
			messages.add("アカウントは10文字以下で入力してください");
		}

		if (!(loginid.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}

		// パスワードの一致確認
		if (!Objects.equals(password, cherkpassword)) {
			// 一致していなかったら、エラーメッセージを表示する
			messages.add("パスワードと確認用パスワードが一致しません");
		}

		//ログインID重複確認

		if (user != null) {
			messages.add("すでに使われているログインIDです");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}