package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Edit;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<Edit> users = new UserService().getUser();
		request.setAttribute("inputusers", users);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Edit> users = new UserService().getUser();
		String id = request.getParameter("id");
		String use_data = request.getParameter("use_data");

		HttpSession session = request.getSession();
		{
			new UserService().chenge(id, use_data);
			response.sendRedirect("./");
			session.setAttribute("users", users);

		}
	}
}
