package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter6.beans.User;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User users) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", loginid");
			sql.append(", branch");
			sql.append(", password");
			sql.append(", department");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // loginid
			sql.append(", ?"); // branch
			sql.append(", ?"); // password
			sql.append(", ?"); // department
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, users.getAccount());
			ps.setString(2, users.getLoginid());
			ps.setInt(3, users.getBranch());
			ps.setString(4, users.getPassword());
			ps.setInt(5, users.getDepartment());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public int checked(Connection connection, String loginid) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT loginid  FROM users where loginid = ? ";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginid);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return 1;
				/* 行からデータを取得 */
			}

			return 0;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}