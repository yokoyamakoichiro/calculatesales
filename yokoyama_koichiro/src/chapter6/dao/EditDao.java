package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter6.beans.Edit;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class EditDao {
	public void insert(Connection connection, Edit users) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", loginid");
			sql.append(", branch_name");
			sql.append(", department_name");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // loginid
			sql.append(", ?"); // branch_name
			sql.append(", ?"); // password
			sql.append(", ?"); // department_name

			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, users.getAccount());
			ps.setString(2, users.getLoginid());
			ps.setString(3, users.getBranch_name());
			ps.setString(5, users.getDepartment_name());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Edit getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Edit ret = toUser(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private Edit toUser(ResultSet rs) throws SQLException {

		Edit ret = new Edit();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String loginid = rs.getString("loginid");
				int branch = rs.getInt("branch");
				String password = rs.getString("password");
				int department = rs.getInt("department");

				Edit user = new Edit();
				user.setId(id);
				user.setAccount(account);
				user.setLoginid(loginid);
				user.setBranch(branch);
				user.setPassword(password);
				user.setDepartment(department);

				return user;
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, Edit user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" account = ?");
			sql.append(", loginid = ?");
			sql.append(", branch = ?");
			sql.append(", password = ?");
			sql.append(", department = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getLoginid());
			ps.setInt(3, user.getBranch());
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getDepartment());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void update2(Connection connection, Edit user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" account = ?");
			sql.append(", loginid = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getLoginid());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDepartment());
			ps.setInt(5, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public Edit idchecked(Connection connection, String checkloginid, int checkid) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append(" WHERE");
			sql.append(" loginid = ?");
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, checkloginid);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String loginid = rs.getString("loginid");
				int use_data = rs.getInt("use_data");
				String branch = rs.getString("branch");
				String department = rs.getString("department");
				if (checkid == id) {
					return null;
				}

				Edit users = new Edit();
				users.setId(id);
				users.setAccount(account);
				users.setLoginid(loginid);
				users.setUse_data(use_data);
				users.setBranch_name(branch);
				users.setDepartment_name(department);
				return users;
			}
			return null;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
