package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Edit;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class InputListDao {

	public List<Edit> getInputList(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.account, ");
			sql.append("users.loginid, ");
			sql.append("users.use_data, ");
			sql.append("branchs.branch_name, ");
			sql.append("departments.department_name  ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branchs ");
			sql.append("ON users.branch = branchs.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department = departments.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Edit> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Edit> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<Edit> ret = new ArrayList<Edit>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String loginid = rs.getString("loginid");
				int use_data = rs.getInt("use_data");
				String branch_name = rs.getString("branch_name");
				String department_name = rs.getString("department_name");
				Edit users = new Edit();

				users.setId(id);
				users.setAccount(account);
				users.setLoginid(loginid);
				users.setUse_data(use_data);
				users.setBranch_name(branch_name);
				users.setDepartment_name(department_name);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public static User changed(Connection connection, String id, String use_data) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" use_data = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, use_data);
			ps.setString(2, id);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

		return null;

	}
}