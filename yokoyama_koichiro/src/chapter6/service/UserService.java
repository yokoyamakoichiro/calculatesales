package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Edit;
import chapter6.beans.User;
import chapter6.dao.InputListDao;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Edit> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			InputListDao inputListDao = new InputListDao();
			List<Edit> users = inputListDao.getInputList(connection);
			commit(connection);

			return users;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public User chenge(String id, String use_data) {
		Connection connection = null;
		try {
			connection = getConnection();

			new InputListDao();
			User ret = InputListDao.changed(connection, id, use_data);

			commit(connection);
			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public int check(String loginid) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

			int User = userDao.checked(connection, loginid);
			commit(connection);
			return User;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}
