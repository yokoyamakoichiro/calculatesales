package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import chapter6.beans.Edit;
import chapter6.dao.EditDao;
import chapter6.utils.CipherUtil;

public class EditService {

	public void register(Edit user) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao usereditDao = new EditDao();
			usereditDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Edit getUser(int Id) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao userEditDao = new EditDao();
			Edit user = userEditDao.getUser(connection, Id);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Edit user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			EditDao editDao = new EditDao();
			editDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update2(Edit user) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editDao = new EditDao();
			editDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Edit idcheck(String checkloginid, int checkid) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editDao = new EditDao();

			Edit User = editDao.idchecked(connection, checkloginid, checkid);
			commit(connection);
			return User;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}
