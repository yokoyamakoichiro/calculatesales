package jp.alhinc.yokoyama_koichiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		//Mapを宣言する
		HashMap<String, String> branchMap = new HashMap<>();
		HashMap<String, Long> commodityMap = new HashMap<>();
		//支店定義ファイル読み込み
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			//支店定義ファイルの存在チェック
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				//分割する
				String[] branch = line.split(",");
				// 正規表現によるフォーマットチェック
				if (!branch[0].matches("^\\d{3}$") || branch.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//Putで要素を追加する
				String number;
				String name;
				number = branch[0];
				name = branch[1];
				branchMap.put(number, name);
				//支店コードと売上金額のMapをつくる
				Long value;
				value = 0L;
				commodityMap.put(number, value);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		// 売上ファイルの格納List
		List<Integer> salesNo = new ArrayList<>();
		//フィルターを使って正規表現をマッチさせる
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String str) {
				if (str.matches("^\\d{8}.rcd$")) {
					return true;
				} else {
					return false;
				}
			}
		};
		// ディレクトリ内のファイル一覧を取得
		String path = args[0];
		File dir = new File(path);
		File[] files = dir.listFiles(filter); // dir内のファイルを配列に格納-
		// 売上ファイルの抽出
		for (int i = 0; i < files.length; i++) { // 配列内の要素の数だけループし一覧を取得
			File inputFile = files[i];
			String[] rcdFileNameSplit = inputFile.getName().split("\\."); // "."の前には\\が必要
			salesNo.add(Integer.parseInt(rcdFileNameSplit[0]));
		}
		// 連番エラーのチェック
		for (int i = 0; i < salesNo.size() - 1; i++) {
			if (salesNo.get(i + 1) - salesNo.get(i) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//出力先のファイルを作成
		try {
			File newfile = new File(args[0], "branch.out");
			newfile.createNewFile();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店コードと売上金額を1行ずつ読み取る
		String code;
		String data;
		String badFile;
		try {
			for (File rcdFile : files) {
				FileReader fileReader = new FileReader(rcdFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				code = bufferedReader.readLine();
				data = bufferedReader.readLine();
				badFile = bufferedReader.readLine();
				String num = data;
				long lon = Long.parseLong(num);
				// 売上ファイルの中身が3行以上の場合のエラー
				if (badFile != null) {
					badFile = rcdFile.getName();
					System.out.println(badFile + "のフォーマットが不正です");
					bufferedReader.close();
					return;
				}
				//全支店の支店コード、支店名、合計金額を出力する
				if (!branchMap.containsKey(code)) {
					System.out.println(rcdFile.getName() + "の支店コードが不正です");
					return;
				}
				commodityMap.put(code, (commodityMap.get(code) + lon));
				//合計金額10桁のエラー処理
				Long digit = commodityMap.get(code);
				if (digit.toString().length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
			File out = new File(args[0], "branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(out));
			for (HashMap.Entry<String, String> entry : branchMap.entrySet()) {
				//コマンドライン引数で指定されたディレクトリに支店別集計ファイルを作成する
				bw.write(entry.getKey() + "," + entry.getValue() + "," + (commodityMap.get(entry.getKey())));
				bw.newLine();
			}
			bw.close();
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}

		}

	}
}
